#!/usr/bin/python3


#
# (c) Magnus Andersson
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
#

import requests,sys,json
import argparse,getpass,re
from datetime import datetime

LOGFILE='creategroup.log'

class GListException(Exception):
    pass

class GCreateException(Exception):
    pass

def grouplist(url,user,pw):
    """ Grouplist() creates a list of the current groups by calling the OCS api and returns a list of groupnames."""
    try:
        r=requests.get(url + '/ocs/v1.php/cloud/groups' + '?format=json',auth=(user,pw),headers={'OCS-APIRequest': 'true'})
        r.raise_for_status()
    except requests.exceptions.HTTPError:
        if r.status_code == 401:
            raise GListException('Unable to acess groupfolder API endpoint, unauthorized.')
        else:
            raise GListException('Unable to acess groupfolder API endpoint.')
    except Exception as e:
        raise GListException('Unknown connection exception: ',e)
    try:
        gdata=json.loads(r.text)
    except Exception as e:
        raise GListException('Group list response is not valid json.')
    glst = []
    try:
        for gname in gdata['ocs']['data']['groups']:
            glst.append(gname)
        return glst
    except KeyError:
        raise GListException('Did not get a valid OCS response.')

def creategroup(url,gname,user,pw):
    """ Function that calls the OCS api and creates a group named by the content in variable gname"""
    try:
        r = requests.post(url + '/ocs/v1.php/cloud/groups' + '?format=json', auth=(user, pw),
                      headers={'OCS-APIRequest': 'true'}, data={'groupid': gname})
        r.raise_for_status()
    except requests.exceptions.HTTPError:
        if r.status_code == 401:
            raise GCreateException('Unauthorized')
        else:
            raise GCreateException('Unable to access OCS group api endpoint')
    except Exception as e:
        raise GCreateException('Connection exception: ',e)


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-u","--user",required=True,help="OCS API Admin user.")
    ap.add_argument("-f","--fqdn",required=True,help="URL of the Nextcloud host")
    ap.add_argument("-d","--dryrun",action='store_true',help="Test and list what changes would have been made.")
    ap.add_argument("grouplistfile",help="Name of file that contains list of groups to create.")
    try:
        args = vars(ap.parse_args())
    except Exception as e:
        print("Exception: ",type(e))
        sys.exit(1)
    passwd = getpass.getpass("Enter password for user " + args['user'] + ': ')
    urlmatch = re.search(r"https?://[^/]*",args['fqdn'])
    if urlmatch:
        ncurl = urlmatch.group()
    else:
        ncurl = args['fqdn']
    print("User: ",args['user'])
    print("Host: ",ncurl)
    try:
        glist=grouplist(ncurl,args['user'],passwd)
    except GListException as e:
        print("Unable to create list of groups. Reason: ",e)
        sys.exit(1)
    try:
        logfile = open(LOGFILE,'a')
    except Exception as e:
        print('Unable to open logfile.Will log to stderr.')
        print("ExceptionType: ",type(e))
        logfile = sys.stderr
    if not args['dryrun']:
        print("******************************************************", file=logfile)
        print("Date and time: ", datetime.now(), file=logfile)
        print("create groups defined in file ", args['grouplistfile'], file=logfile)
        print("API User: ", args['user'], file=logfile)
        print("Url: ", ncurl, file=logfile)
        print("******************************************************", file=logfile)
    try:
        f = open(args['grouplistfile'],'r')
    except Exception as e:
        print('Exception type: ',type(e))
        sys.exit(1)
    with f as gfile:
        try:
            for line in gfile:
                if line.strip() not in glist:
                    if args['dryrun']:
                        print('Group ' + line.strip() + ' would have been created')
                    else:
                        try:
                            creategroup(ncurl,line.strip(),args['user'],passwd)
                        except GCreateException as e:
                            print("Unable to create group: ",e)
                            sys.exit(1)
                        print("Group " + line.strip() + ' created.')
                        print(datetime.now()," Group " + line.strip() + ' created.',file=logfile)
                else:
                    if args['dryrun']:
                        print("Group " + line.strip() + " does already exist. Creation of group would have been skipped.")
                    else:
                        print("Group " + line.strip() + " does already exist. Skip creation of group")
                        print(datetime.now, " Group " + line.strip() + " does already exist. Skip creation of group",file=logfile)
        except Exception as e:
            print('Exception type: ',type(e))
            print('Unable to create group. Exit!')
            sys.exit(1)

if __name__ == '__main__':
    main()